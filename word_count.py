import sys

def word_counter(filename):
    word_count = {}
    with open(filename, "r") as file:
        text = file.read()
    text = text.lower()
    text = text.split()
    for word in text:
        word_count[word] = text.count(word)
    return word_count


def print_words(filename):
    word_count = word_counter(filename)
    for w in sorted(word_count.keys()):
        print(w, word_count[w])


def print_top(filename):
    word_count = word_counter(filename)
    word_count = sorted([(value,key) for (key,value) in word_count.items()], reverse=True)
    word_count = word_count[:20]
    for w in word_count:
        print(w[1], w[0])


def main():
    if len(sys.argv) != 3:
        print("usage: ./wordcount.py {--count | ---topcount} file")
        sys.exit(1)

    option = sys.argv[1]
    filename = sys.argv[2]
    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)

    else:
        print("unknown option: " + option)
        sys.exit(1)


if __name__ == '__main__':
    main()